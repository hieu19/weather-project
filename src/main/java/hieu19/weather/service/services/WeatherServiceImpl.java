package hieu19.weather.service.services;

import hieu19.weather.model.air_quality.WeatherData;
import hieu19.weather.util.IOUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class WeatherServiceImpl {

    @Value("${openweathermap.api.url}")
    private String currentWeatherAPIURL;

    @Value("${openweathermap.api.url.air.pollution}")
    private String currentAirQualityAPIURL;

    @Value("${openweathermap.api.key}")
    private String apiKey;

    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private ResourceLoader resourceLoader;
    @Autowired
    private ModelMapper modelMapper;

    @Cacheable(value = "weather", key = "#city")
    public Object getCurrentWeather(String city) {
        String url = currentWeatherAPIURL + "?q=" + city + "&appid=" + apiKey + "&units=metric";
        try {
            System.out.println("cache miss for city: " + city);
            Object response = restTemplate.getForObject(url, Object.class);
            return response;
        } catch (Exception ex) {
            return null;
        }
    }

    @Cacheable(value = "air", key = "#city")
    public WeatherData getCurrentAirQuality(String city) {
        String url = currentAirQualityAPIURL + getLatitudeAndLongitudeByCity(city) + "&appid=" + apiKey;
        try {
            WeatherData weatherData = restTemplate.getForObject(url, WeatherData.class);
            return weatherData;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    private String getLatitudeAndLongitudeByCity(String city) {
        List<String[]> csvData;
        String lon = null;
        String lat = null;
        csvData = IOUtils.readCSV("classpath:static/destination.csv", resourceLoader);
        for (String[] string : csvData) {
            if (city.toLowerCase().equalsIgnoreCase(string[0].toLowerCase())) {
                lat = string[1];
                lon = string[2];
            }
        }
        String result = "?lat=" + lat + "&lon=" + lon;
        return result;
    }
}
