package hieu19.weather.model.air_quality;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class WeatherData {
    private Coord coord;
    private List<WeatherInfo> list;
}
@Getter
@Setter
@NoArgsConstructor
class Coord {
    private double lon;
    private double lat;
}
@Getter
@Setter
@NoArgsConstructor
class WeatherInfo {
    private Main main;
    private Components components;
    private long dt;
}
@Getter
@Setter
@NoArgsConstructor
class Main {
    private int aqi;
}
@Getter
@Setter
@NoArgsConstructor
class Components {
    private double co;
    private double no;
    private double no2;
    private double o3;
    private double so2;
    private double pm2_5;
    private double pm10;
    private double nh3;
}
