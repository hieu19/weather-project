package hieu19.weather.model.air_quality;

import java.util.Date;

public class AirQualityResponseDTO {
    private String location;
    private String air_quality;
    private Date datetime;
    private double co;
    private double no;
    private double no2;
    private double o3;
    private double so2;
    private double pm2_5;
    private double pm10;
    private double nh3;
}
