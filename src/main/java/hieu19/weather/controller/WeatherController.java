package hieu19.weather.controller;

import hieu19.weather.model.air_quality.WeatherData;
import hieu19.weather.service.services.WeatherServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/weather")
public class WeatherController {
    @Autowired
    private WeatherServiceImpl weatherService;

    public WeatherController(WeatherServiceImpl weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("/{city}")
    public ResponseEntity getCurrentWeather(@PathVariable String city) {
        Object weatherData = weatherService.getCurrentWeather(city);
        return weatherData != null ? ResponseEntity.ok(weatherData) : ResponseEntity.ok("city not found");
    }

    @GetMapping("/air/{city}")
    public ResponseEntity getCurrentAirQuality(@PathVariable String city) {
        WeatherData weatherData = weatherService.getCurrentAirQuality(city);
        return weatherData != null ? ResponseEntity.ok(weatherData) : ResponseEntity.ok("city not found");
    }
}
