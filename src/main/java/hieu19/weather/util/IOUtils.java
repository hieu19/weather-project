package hieu19.weather.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class IOUtils {

    public static List<String[]> readCSV(String path, ResourceLoader resourceLoader) {
        Resource resource = resourceLoader.getResource(path);
        // Open an InputStream from the resource
        try (InputStream inputStream = resource.getInputStream(); BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            // Read CSV content
            List<String[]> csvData = new ArrayList<>();
            String line;
            while ((line = reader.readLine()) != null) {
                // Assuming CSV fields are comma-separated
                String[] fields = line.split(",");
                csvData.add(fields);
            }
            return csvData;
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return null;
    }
}
